package br.edu.ifpr.pedro.aula7_sqlite

import android.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import androidx.room.Room
import br.edu.ifpr.pedro.aula7_sqlite.db.AppDataBase
import br.edu.ifpr.pedro.aula7_sqlite.db.dao.PersonDao
import br.edu.ifpr.pedro.aula7_sqlite.entities.Person
import kotlinx.android.synthetic.main.activity_main.*
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    lateinit var personDao: PersonDao
    lateinit var adapter: ArrayAdapter<Person>
    var personEditing:Person? = null
    var i=0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        sumirBotaoExcluir()

        val db = Room.databaseBuilder(applicationContext, AppDataBase::class.java, "person.db")
            .allowMainThreadQueries()
            .build()
        personDao = db.personDao()
        personDao.getAll()

        this.loadData()
        btSalvar.setOnClickListener { savePerson() }

        btExcluir.setOnClickListener {
            deletePerson(this!!.personEditing!!)
            sumirBotaoExcluir()
        }

        btNovo.setOnClickListener{ newPerson() }

        listPeople.setOnItemClickListener { _, _, position , _ ->
            val person = adapter.getItem(position) as Person
            editPerson(person)
            btExcluir.setVisibility(View.VISIBLE);
        }

        listPeople.setOnItemLongClickListener { _, _, position , _ ->
            val person = adapter.getItem(position) as Person
            sumirBotaoExcluir()
            i=1
            editPerson(person)
            savePerson()
            updatePerson(getPersonFromList(position))
            true
        }
    }

    private fun getPersonFromList(position :Int)=adapter.getItem(position) as Person

    private fun sumirBotaoExcluir(){btExcluir.setVisibility(View.INVISIBLE)}

    private fun newPerson(){
        sumirBotaoExcluir()
        clear()
        personEditing = null
        txtFirstName.requestFocus()
    }

    private fun exemplo_simples() {
        //Cria o gerador do AlertDialog
        val builder = AlertDialog.Builder(this)
        //define o titulo
        builder.setTitle("Titulo")
        //define a mensagem
        builder.setMessage("Qualifique este software")
        //define um botão como positivo
        builder.setPositiveButton(
            "Positivo"
        ) { arg0, arg1 -> Toast.makeText(this@MainActivity, "positivo=$arg1", Toast.LENGTH_SHORT).show() }
        //define um botão como negativo.
        builder.setNegativeButton(
            "Negativo"
        ) { arg0, arg1 -> Toast.makeText(this@MainActivity, "negativo=$arg1", Toast.LENGTH_SHORT).show() }
        //cria o AlertDialog
        val alerta = builder.create()
        //Exibe
        alerta.show()
    }

    private fun deletePerson(person: Person){
        AlertDialog.Builder(this)
            .setTitle("Deletando tarefa")
            .setMessage("Tem certeza que deseja deletar essa tarefa?")
            .setPositiveButton("sim"
            ) { dialogInterface, i ->
        adapter.notifyDataSetChanged()
        personDao.remove(person)
        loadData()
        }
        .setNegativeButton("não", null)
        .show()
    }

    private fun updatePerson(person: Person){
        personDao.update(person)
        loadData()
    }

    private fun editPerson(person:Person){
        txtFirstName.setText(person.firstName)
        txtLastName.setText(person.lastName)

        if (i==1) {
            if (person.title.equals("[FEITA]")){
                person.title= "[NÃO FEITA]"
            }

            else{
                person.title= "[FEITA]"}
            i=0
        }
        personEditing = person
    }

    private fun savePerson(){
        val firstName = txtFirstName.text.toString().toUpperCase()
        val lastName = txtLastName.text.toString().toUpperCase()

        if (txtFirstName.text.toString().equals("")){
            Toast.makeText(applicationContext, "Informe o título!!!", Toast.LENGTH_SHORT).show()
            txtFirstName.requestFocus()
        }
        else
        if (txtLastName.text.toString().equals("")){
            Toast.makeText(applicationContext, "Descreva a tarefa!!!", Toast.LENGTH_SHORT).show()
            txtLastName.requestFocus()
        }
        else{
            if (personEditing != null){
                personEditing?.let { person ->
                    person.firstName = firstName
                    person.lastName = lastName

                    personDao.update(person)}

                Toast.makeText(applicationContext, "Tarefa alterada com sucesso!!!", Toast.LENGTH_SHORT).show()
            }else{

                val person = Person(firstName, lastName,"[NÃO FEITA]")
                personDao.insert(person)
                Toast.makeText(applicationContext, "Tarefa inserida com sucesso!!!", Toast.LENGTH_SHORT).show()
                txtFirstName.requestFocus()
            }
            loadData()

        }

    }

    private fun loadData(){
        val people = personDao.getAll()
        adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, people)
        listPeople.adapter = adapter
        clear()
    }

    private fun clear(){
        txtFirstName.setText("")
        txtLastName.setText("")
        personEditing = null
    }

}
